FROM httpd:2.4

ENV PHP_HOST php-fpm
ENV PHP_PORT 9000
ENV PHP_PATH /var/www/html
ENV DISALLOW_ROBOTS true
ENV HT_PASSORWD ""
ENV HT_LOGIN anonymous
ENV PROXY_TIMEOUT 60

# ACTIVATE rewrite, proxy, proxy_http, proxy_fcgi, deflate & expires modules.
RUN sed -i \
  -e "s/#LoadModule rewrite_module/LoadModule rewrite_module/g" \
  -e "s/#LoadModule proxy_module/LoadModule proxy_module/g" \
  -e "s/#LoadModule proxy_http_module/LoadModule proxy_http_module/g" \
  -e "s/#LoadModule proxy_fcgi_module/LoadModule proxy_fcgi_module/g" \
  -e "s/#LoadModule deflate_module/LoadModule deflate_module/g" \
  -e "s/#LoadModule expires_module/LoadModule expires_module/g" \
  /usr/local/apache2/conf/httpd.conf

# Set default performance configuration
COPY config/performance.conf /usr/local/apache2/conf/performance.conf
RUN echo "Include conf/performance.conf" >> /usr/local/apache2/conf/httpd.conf

# Include our vhost with php-fpm
COPY config/vhost.conf /usr/local/apache2/vhost.conf
RUN echo "ServerName localhost" >> /usr/local/apache2/conf/httpd.conf \
  && echo "Include conf/extra/vhost.conf" >> /usr/local/apache2/conf/httpd.conf

# Generate robots.txt file to disallow search engine accesses
RUN { \
    echo 'User-agent: *'; \
    echo 'Disallow: /'; \
  } > /usr/local/apache2/robots.txt

# Change entrypoint
COPY config/docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

CMD ["httpd-foreground"]
