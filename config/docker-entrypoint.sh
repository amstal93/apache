#!/bin/bash
set -e

# This copy will avoid using preconfigured files.
# Environment changes will be done at each runs.
cp /usr/local/apache2/vhost.conf /usr/local/apache2/conf/extra/vhost.conf

path=${PHP_PATH%/}
mkdir -p $path

sed -i \
 -e "s|{PHP_HOST}|$PHP_HOST|g" \
 -e "s|{PHP_PORT}|$PHP_PORT|g" \
 -e "s|{PHP_PATH}|$PHP_PATH|g" \
 -e "s|{PROXY_TIMEOUT}|$PROXY_TIMEOUT|g" \
 /usr/local/apache2/conf/extra/vhost.conf

if [ $DISALLOW_ROBOTS = "true" ]; then
  sed -i "s|#R||g" /usr/local/apache2/conf/extra/vhost.conf
fi

if (! [ -z "$HT_PASSWORD" ]) then
  htpasswd -b -c /usr/local/apache2/.htpasswd $HT_LOGIN $HT_PASSWORD
  sed -i \
    -e "s|#A||g" \
    -e "s|Require all granted||g" \
    /usr/local/apache2/conf/extra/vhost.conf
fi

exec "$@"
